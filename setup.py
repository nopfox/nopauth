from setuptools import setup, find_packages

setup(
    name='NopAuth',
    version='1.0.0',
    author='Zaxbbun Du',
    author_email='zaxbbun@gmail.com',
    license='MIT',
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
        "httpie"
    ],
    entry_points={
        'httpie.plugins.auth.v1': [
            'NopAuthPlugin = NopAuth:NopAuthPlugin'
        ]
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Programming Language :: Python',
        'Intended Audience :: Developers',
        'Environment :: Plugins',
        'License :: OSI Approved :: MIT License',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Utilities'
    ],
)
