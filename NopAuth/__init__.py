"""
Authorization Plugin for NOP
"""

import hmac
import time
import uuid
import hashlib
import requests
import httpie.plugins


class NopAuth(requests.auth.HTTPBasicAuth):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __call__(self, r):
        timestamp = str(int(time.time()))
        nonce = uuid.uuid4().hex
        esc = chr(0x1B)

        if r.body and isinstance(r.body, bytes):
            r.body = str(r.body, "utf-8")

        msg = esc.join([r.path_url, timestamp, nonce, r.body or ""])

        h = hmac.new(
            bytes(self.password, "utf-8"),
            bytes(msg, "utf-8"),
            digestmod=hashlib.sha1,
        )
        sign = h.hexdigest()

        r.headers["Authorization"] = "NOP %s:%s" % (self.username, sign)
        r.headers["Timestamp"] = timestamp
        r.headers["Nonce"] = nonce

        return r


class NopAuthPlugin(httpie.plugins.AuthPlugin):
    auth_type = "nop"

    def get_auth(self, username=None, password=None):
        return NopAuth(username, password)
